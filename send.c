//
// Created by kohbo on 2/27/2018.
//

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include "mdsp.h"

void sighandler(int param){
    printf("Ctrl+c detected. Exiting...");

    exit(EXIT_SUCCESS);
}

void *send_run(void *arg) {
    struct sockaddr_in addr;
    struct in_addr iface;
    int fd, rc;
    struct mdsp_init *args = (struct mdsp_init *)arg;

    int msgindex = 0;
    char msgs[5][30] = {
        "Hello!",
        "Whats up?",
        "Nothing much.",
        "You?",
        "Just hanging out"
    };

    printf("Starting send thread...\n");

    /* Setup signal handler */
    signal(SIGINT, sighandler);

    /* create what looks like an ordinary UDP socket */
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (fd < 0) {
        perror("socket");
        pthread_exit((void *)EXIT_FAILURE);
    }

    /* Set up interface */
    memset(&iface, 0, sizeof(iface));
    iface.s_addr = inet_addr(args->local_ip);

    rc = setsockopt(fd, IPPROTO_IP, IP_MULTICAST_IF, (void *) &iface, sizeof(iface));
    if (rc < 0) {
        perror("setsockopt()");
        pthread_exit((void *)EXIT_FAILURE);
    }

    /* set up destination address */
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(HELLO_GROUP);
    addr.sin_port = htons(HELLO_PORT);

    /* now just sendto() our destination! */
    while (1) {
        if (sendto(fd, msgs[msgindex++%5], strlen(msgs[msgindex%5]), 0, (struct sockaddr *) &addr,
                   sizeof(addr)) < 0) {
            perror("sendto");
            pthread_exit((void *)EXIT_FAILURE);
        }
        sleep(1);
    }
}