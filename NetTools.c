//
// Created by kohbo on 2/27/2018.
//

#include <sys/types.h>
#include <ifaddrs.h>
#include <stdio.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netdb.h>

void
getIFs(struct ifaddrs **ifaddr){
    if (getifaddrs(ifaddr) == -1) {
        perror("getifaddrs");
        exit(EXIT_FAILURE);
    }
}

char*
chooseIF(struct ifaddrs **ifaddr){

}

int
main() {
    struct ifaddrs *ifaddr, *ifa;
    char host[NI_MAXHOST];
    int s;

    getifs(&ifaddr);

    for(ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next){
        if(ifa->ifa_addr == NULL || ifa->ifa_addr->sa_family != AF_INET){
            continue;
        }

        printf("%s ", ifa->ifa_name);

        s = getnameinfo(ifa->ifa_addr, sizeof(struct sockaddr_in), host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);

        if (s != 0) {
            printf("getnameinfo() failed: %s\n", gai_strerror(s));
            exit(EXIT_FAILURE);
        }

        printf("\t\taddress: <%s>\n", host);

    }

    freeifaddrs(ifaddr);
    return 0;
}
