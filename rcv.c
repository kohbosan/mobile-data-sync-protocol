//
// Created by kohbo on 2/27/2018.
//

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>
#include <pthread.h>
#include "const.h"
#include "mdsp.h"

struct sockaddr_in localSock;
struct ip_mreq group;
int sd;
char databuf[MAX_LENGTH];

void *rcv_run(void *arg) {
    struct mdsp_init *args = (struct mdsp_init *)arg;

    printf("Starting receive thread...");

    /* Create a datagram socket on which to receive. */
    sd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sd < 0) {
        perror("Opening datagram socket error");
        pthread_exit((void *)EXIT_FAILURE);
    } else {
        printf("Opening datagram socket....OK.\n");
    }

    /* Enable SO_REUSEADDR to allow multiple instances of this
     * application to receive copies of the multicast datagrams.
     */
    {
        int reuse = 1;
        if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, (char *) &reuse, sizeof(reuse)) < 0) {
            perror("Setting SO_REUSEADDR error");
            close(sd);
            pthread_exit((void *)EXIT_FAILURE);
        } else
            printf("Setting SO_REUSEADDR...OK.\n");
    }

    /* Bind to the proper port number with the IP address specified as INADDR_ANY. */
    memset((char *) &localSock, 0, sizeof(localSock));
    localSock.sin_family = AF_INET;
    localSock.sin_port = htons(HELLO_PORT);
    localSock.sin_addr.s_addr = INADDR_ANY;
    if (bind(sd, (struct sockaddr *) &localSock, sizeof(localSock))) {
        perror("Binding datagram socket error");
        close(sd);
        pthread_exit((void *)EXIT_FAILURE);
    } else
        printf("Binding datagram socket...OK.\n");

    /* Join the multicast group HELLO_GROUP on the local LOCAL_IP
     * interface. Note that this IP_ADD_MEMBERSHIP option must be
     * called for each local interface over which the multicast
     * datagrams are to be received.
     */
    group.imr_multiaddr.s_addr = inet_addr(HELLO_GROUP);
    group.imr_interface.s_addr = inet_addr(args->local_ip);
    if (setsockopt(sd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *) &group, sizeof(group)) < 0) {
        perror("Adding multicast group error");
        close(sd);
        pthread_exit((void *)EXIT_FAILURE);
    } else
        printf("Adding multicast group...OK.\n");

    /* Set socket to non-blocking */
    fcntl(sd, O_NONBLOCK, 1);

    /* Poll for data available to be read from socket */
    struct pollfd fds[1];
    fds[0].fd = sd;
    fds[0].events = POLLIN;

    for (_ssize_t rc = 0; rc > -1;) {
        poll(fds, 1, 3000);
        if ((fds[0].revents & POLLIN) == POLLIN) {
            rc = read(sd, databuf, MAX_LENGTH);

            if (rc < 0) {
                perror("Reading datagram message error");
                close(sd);
                pthread_exit((void *)EXIT_FAILURE);
            }

            // Handle the packet data
            databuf[rc] = '\0';
            printf("Multicast Message: \"%s\"\n", databuf);
        }
    }

    return NULL;
}