//
// Created by kohbo on 3/7/2018.
//

#ifndef MOBILE_DATA_SYNC_PROTOCOL_MDSP_H
#define MOBILE_DATA_SYNC_PROTOCOL_MDSP_H

#include "const.h"
#include "send.h"
#include "rcv.h"

struct node_data {
    int x;
    int y;
    int z;
};

struct data_collection {
    
};

struct mdsp_init {
    char *local_ip;
};

#endif //MOBILE_DATA_SYNC_PROTOCOL_MDSP_H
