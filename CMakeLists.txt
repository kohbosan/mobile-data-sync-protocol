cmake_minimum_required(VERSION 3.6)
project(Mobile_Data_Sync_Protocol)

set(CMAKE_C_STANDARD 99)
set(CMAKE_C_COMPILER "C:/Dev/cygwin64/bin/gcc.exe")
set(CMAKE_CXX_COMPILER "C:/Dev/cygwin64/bin/g++.exe")

set(SOURCE_FILES demo.c)
set(LIB_SRC_FILES mdsp.c rcv.c send.c)

add_library(mdsp STATIC ${LIB_SRC_FILES})

add_executable(mdsp_demo ${SOURCE_FILES})
target_link_libraries(mdsp_demo mdsp)
