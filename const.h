//
// Created by kohbo on 2/27/2018.
//

#ifndef CSCRATCH2_CONSTS_H
#define CSCRATCH2_CONSTS_H

#define MAX_LENGTH 1024
#define HELLO_PORT 60055
#define HELLO_GROUP "239.0.0.37"

#endif //CSCRATCH2_CONSTS_H