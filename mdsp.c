#include <stdio.h>
#include <pthread.h>
#include "mdsp.h"

int main(int argc, char *argv[])
{
    pthread_t rcv_t;
    pthread_t send_t;
    struct mdsp_init args;

    if (argc != 2)
    {
        printf("Usage mdsp.exe <IP>");
        return 1;
    }

    args.local_ip = argv[1];

    // pthread_create(&rcv_t, NULL, rcv_run, NULL);
    pthread_create(&send_t, NULL, send_run, (void *)&args);

    // pthread_join(rcv_t, NULL);
    pthread_join(send_t, NULL);
}